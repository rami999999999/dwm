#!/bin/bash
spacer="   "

while true;
do
	STR=""
	
	# Volume
	mute="$( pamixer --get-mute )"

	if [ "$mute" == "false" ]; then
		vol="$( pamixer --get-volume )"
		STR+="|  vol $vol%"
	else
		STR+="|  muted"
	fi
	STR+="$spacer"

	# Backlight
#	STR+="$( xbacklight | awk -F "." '{print "| scr " $1 "%" }' )"
#	STR+="$spacer"

	# Temp
	sum=0
	sens="$( sensors | awk -F '+|°|\n' '/Core/ { sum += $2 } END { printf "%3.0d", sum / 4 }' )"

	STR+="| "$sens"°C"$spacer

	# Date/Time
	STR+="$( date | awk -F ':| ' '{print "|  " $1 ", " $3 " " $4 ", " $9 "   |  " $5 ":" $6 }' )"
	STR+="  "
	
	xsetroot -name "$STR"
	sleep 1
done
